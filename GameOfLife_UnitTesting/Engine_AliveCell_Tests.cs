using Xunit;
using FluentAssertions;
using GameOfLife;

namespace GameOfLife_Test
{
    public class Engine_AliveCell_Tests
    {
        private Engine Engine;

        [Fact]
        public void AliveCell_WithZeroNeighbor_Should_Die()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 0, 0 },
                    { 0, 1, 0 },
                    { 0, 0, 0 },
            });

            this.Engine.World[1, 1].Should().Be(1);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(0);
        }

        [Fact]
        public void AliveCell_WithOneNeighbor_Should_Die()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 0, 0 },
                    { 1, 1, 0 },
                    { 0, 0, 0 },
            });

            this.Engine.World[1, 1].Should().Be(1);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(0);
        }

        [Fact]
        public void AliveCell_WithTwoNeighbors_Should_RemainAlive()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 0, 1 },
                    { 1, 1, 0 },
                    { 0, 0, 0 },
            });

            this.Engine.World[1, 1].Should().Be(1);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(1);
        }

        [Fact]
        public void AliveCell_WithThreeNeighbors_Should_RemainAlive()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 0, 1 },
                    { 1, 1, 0 },
                    { 0, 1, 0 },
            });

            this.Engine.World[1, 1].Should().Be(1);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(1);
        }

        [Fact]
        public void AliveCell_WithFourNeighbors_Should_Die()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 0, 1 },
                    { 1, 1, 1 },
                    { 0, 1, 0 },
            });

            this.Engine.World[1, 1].Should().Be(1);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(0);
        }
    }
}
