using Xunit;
using FluentAssertions;
using GameOfLife;

namespace GameOfLife_Test
{
    public class Engine_DeadCell_Tests
    {
        private Engine Engine;

        [Fact]
        public void DeadCell_WithZeroNeighbor_Should_RemainDead()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 0, 0 },
                    { 0, 0, 0 },
                    { 0, 0, 0 },
            });

            this.Engine.World[1, 1].Should().Be(0);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(0);
        }

        [Fact]
        public void DeadCell_WithOneNeighbor_Should_RemainDead()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 0, 0 },
                    { 0, 0, 1 },
                    { 0, 0, 0 },
            });

            this.Engine.World[1, 1].Should().Be(0);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(0);
        }

        [Fact]
        public void DeadCell_WithTwoNeighbors_Should_RemainDead()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 0, 0 },
                    { 0, 0, 1 },
                    { 1, 0, 0 },
            });

            this.Engine.World[1, 1].Should().Be(0);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(0);
        }

        [Fact]
        public void DeadCell_WithThreeNeighbors_Should_BeBorn()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 1, 0 },
                    { 0, 0, 1 },
                    { 1, 0, 0 },
            });

            this.Engine.World[1, 1].Should().Be(0);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(1);
        }

        [Fact]
        public void DeadCell_WithFourNeighbors_Should_RemainDead()
        {
            this.Engine = Engine.Of(new byte[,]
            {
                    { 0, 1, 0 },
                    { 1, 0, 1 },
                    { 1, 0, 0 },
            });

            this.Engine.World[1, 1].Should().Be(0);

            this.Engine.ProcessNewGeneration();

            this.Engine.World[1, 1].Should().Be(0);
        }
    }
}
