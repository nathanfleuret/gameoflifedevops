﻿using GameOfLife;
using GameOfLifeDevops.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace GameOfLifeDevops.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private Engine _engine;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
            _engine = Engine.Of(20, 20, true); // Changer la taille du monde pour feedback visuel sur le site
        }

        public IActionResult Index()
        {
            return View(_engine);
        }

        [HttpPost]
        public IActionResult NewGeneration(string worldAsString)
        {
            _engine = Engine.Of(Engine.ToByteArray(worldAsString));
            _engine.ProcessNewGeneration();
            return View("Index", _engine);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
